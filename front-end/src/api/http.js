import axios from 'axios';
axios.interceptors.request.use( (config) => {

    config.headers.pskey=localStorage.getItem("pskey")
    return config
 })

export default axios
