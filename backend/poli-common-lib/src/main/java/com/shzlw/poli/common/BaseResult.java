package com.shzlw.poli.common;

import java.io.Serializable;
//用于feign的返回结果封装
public class BaseResult<T>  implements Serializable{
    private Boolean result ;
    private T data ;
    private String msg ;

    public Boolean getResult() {
        return result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setResult(Boolean result) {
        this.result = result;

    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
