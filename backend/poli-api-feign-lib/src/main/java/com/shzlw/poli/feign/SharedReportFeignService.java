package com.shzlw.poli.feign;

import com.shzlw.poli.common.BaseResult;
import com.shzlw.poli.dto.SharedLinkInfo;
import com.shzlw.poli.util.Constants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@FeignClient(name = Constants.POLI_API_SERVER, fallback = SharedReportFeignServiceFallback.class)
public interface SharedReportFeignService {

    @RequestMapping(value = "/ws/sharedreport/getSharedLinkInfoByShareKey", method = RequestMethod.POST)
    public BaseResult<SharedLinkInfo> getSharedLinkInfoByShareKey(@RequestHeader("shareKey") String shareKey);
}
