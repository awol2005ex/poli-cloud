package com.shzlw.poli.feign;

import com.shzlw.poli.common.BaseResult;
import com.shzlw.poli.dto.SharedLinkInfo;
import org.springframework.stereotype.Component;

@Component
public class SharedReportFeignServiceFallback implements  SharedReportFeignService {
    @Override
    public BaseResult<SharedLinkInfo> getSharedLinkInfoByShareKey(String shareKey) {
        BaseResult<SharedLinkInfo> ret = new BaseResult<>();
        ret.setResult(false);
        ret.setData(null);
        ret.setMsg("API调用失败");
        return ret;
    }
}
