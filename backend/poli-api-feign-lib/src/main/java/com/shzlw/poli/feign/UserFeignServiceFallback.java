package com.shzlw.poli.feign;

import com.shzlw.poli.common.BaseResult;
import com.shzlw.poli.model.User;
import org.springframework.stereotype.Component;

@Component
public  class UserFeignServiceFallback implements UserFeignService {
    @Override
    public BaseResult<User> getUserBySessionKey(String sessionKey) {
        BaseResult<User> ret = new BaseResult<>();
        ret.setResult(false);
        ret.setData(null);
        ret.setMsg("API调用失败");
        return ret;
    }

    @Override
    public BaseResult<User> getUserByApiKey(String apiKey) {
        BaseResult<User> ret = new BaseResult<>();
        ret.setResult(false);
        ret.setData(null);
        ret.setMsg("API调用失败");
        return ret;
    }
}
