package com.shzlw.poli.feign;

import com.shzlw.poli.common.BaseResult;
import com.shzlw.poli.model.User;
import com.shzlw.poli.util.Constants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = Constants.POLI_API_SERVER, fallback = UserFeignServiceFallback.class)
public interface UserFeignService {
    //根据session key(pskey) 获取当前用户
    @RequestMapping(value = "/ws/user/getUserBySessionKey", method = RequestMethod.POST)
    public BaseResult<User> getUserBySessionKey(@RequestHeader(Constants.SESSION_KEY) String sessionKey);

    @RequestMapping(value = "/ws/user/getUserByApiKey", method = RequestMethod.POST)
    public BaseResult<User> getUserByApiKey(@RequestHeader(Constants.HTTP_HEADER_API_KEY)  String apiKey);
}
