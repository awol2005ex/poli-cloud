package com.shzlw.poli.web;

import com.shzlw.poli.feign.UserFeignService;
import com.shzlw.poli.model.User;
import com.shzlw.poli.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/*转换网关客户端IP到服务端*/
@Component
@Slf4j
public class XForwardedForFilter implements GlobalFilter, Ordered {
    private static final String HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";


    @Autowired
    UserFeignService userFeignService ;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        //log.info("request = {}", JSONArray.toJSONString( exchange.getRequest()) );
        //向headers中放文件，记得build
        ServerHttpRequest host = exchange.getRequest().mutate().header(HTTP_X_FORWARDED_FOR, getRemoteHost(exchange.getRequest())).build();
        //将现在的request 变成 change对象
        ServerWebExchange build = exchange.mutate().request(host).build();

        /*to do 根据jwt过滤链接--start*/

        String path =exchange.getRequest().getPath().value();
        if(path!=null && path.startsWith("/api/ws/")){
            String sessionKey =exchange.getRequest().getHeaders().getFirst(Constants.SESSION_KEY);
            if(sessionKey!=null){
                User user = userFeignService.getUserBySessionKey(sessionKey).getData();
                if (user == null) {
                    return to401(exchange);
                }
            } else {
                return to401(exchange);
            }
        }



        /*to do 根据jwt过滤链接--end*/
        return chain.filter(build);
    }

    private Mono<Void> to401(ServerWebExchange exchange){
        ServerHttpResponse originalResponse = exchange.getResponse();
        originalResponse.setStatusCode(HttpStatus.OK);
        originalResponse.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        byte[] response = "{\"code\": \"401\",\"msg\": \"401 Unauthorized.\"}"
                .getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = originalResponse.bufferFactory().wrap(response);
        return originalResponse.writeWith(Flux.just(buffer));
    }

    @Override
    public int getOrder() {
        return 0;
    }
    private String getRemoteHost(ServerHttpRequest request){
        String ip = request.getHeaders().getFirst("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeaders().getFirst("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeaders().getFirst("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getRemoteAddress().toString();
        }
        return ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
    }
}
