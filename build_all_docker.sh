cd backend/poli-api-server
gradle bootJar
docker build --no-cache -t poli/poli-api-server .
docker tag poli/poli-api-server 10.110.36.168:5000/poli/poli-api-server
docker push 10.110.36.168:5000/poli/poli-api-server
cd ../poli-gateway-server
gradle bootJar
docker build --no-cache -t poli/poli-gateway-server .
docker tag poli/poli-gateway-server 10.110.36.168:5000/poli/poli-gateway-server
docker push 10.110.36.168:5000/poli/poli-gateway-server
cd ../../front-end
npm run build
docker build --no-cache -t poli/poli-front-end .
docker tag poli/poli-front-end 10.110.36.168:5000/poli/poli-front-end
docker push 10.110.36.168:5000/poli/poli-front-end
cd ../common/poli-admin-server
gradle bootJar
docker build --no-cache -t poli/poli-admin-server .
docker tag poli/poli-admin-server 10.110.36.168:5000/poli/poli-admin-server
docker push 10.110.36.168:5000/poli/poli-admin-server
cd ../..

